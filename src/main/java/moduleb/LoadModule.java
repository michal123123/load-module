package moduleb;

import com.jcraft.jsch.*;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.apache.http.protocol.HTTP.USER_AGENT;

public class LoadModule {
    public static String WORKSPACE = System.getenv("OPENSTACK_WORKSPACE");
    private static String host;
    private static String user;
    private static String passwd;
    private static int port = 22;
    private static InputStream in;
    private static OutputStream out;
    private static File rsaKeyFile =
            new File(WORKSPACE + "/keyPair");
    private static File lockFile =
            new File(WORKSPACE + "/lock");
    private static File ids =
            new File(WORKSPACE + "/ids");
    private static byte[] tmp = new byte[5000];
    private static PropertiesFromFile properties;
    private static Logger LOGGER = Logger.getLogger(LoadModule.class);

    public static void main(String[] args) throws InterruptedException, IOException {
        LOGGER.info("Module B starting main function");
        obtainLock();
        if (!rsaKeyFile.exists() || !ids.exists()) {
            LOGGER.error("Module B conf files not found");
            throw new IllegalStateException();
        }
        properties = PropertiesFromFile.getInstance(ids);
        user = PropertiesFromFile.getProperty(PropertiesFromFile.USERNAME);
        passwd = PropertiesFromFile.getProperty(PropertiesFromFile.PASSWD);
        passwd = PropertiesFromFile.getProperty(PropertiesFromFile.HOST);
        int argument = CpuLoadFinder.findCpuForDateTime(WORKSPACE + "/pastUsageData",
                LocalDateTime.now().withMinute(0).withSecond(0).withNano(0));
        LOGGER.info("Sum of cpu usage is " + argument);
        try {
            LOGGER.info("Connecting to " + user + "@" + host + ":" + port);
            JSch jsch = new JSch();

            Session session = jsch.getSession(user, host, port);
            session.setPassword(passwd);
            UserInfo ui = new SshUserInfo();
            session.setUserInfo(ui);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(10_000);
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand("sudo -S -p '' su");
            in = channel.getInputStream();
            out = channel.getOutputStream();
            ((ChannelExec) channel).setErrStream(System.err);
            channel.connect();
            readOutput();
            writeCommand(passwd);
            readOutput();
            writeCommand("su stack");
            readOutput();
            writeCommand("whoami");
            readOutput();
            writeCommand("cd /opt/stack/devstack");
            readOutput();
            writeCommand("pwd");
            readOutput();
            writeCommand(". openrc admin demo");
            Thread.sleep(5_000);
            readOutput();
            ///token=======================================================================================
            LOGGER.info("Executing openstack token issue");
            writeCommand("openstack token issue");
            Thread.sleep(10_000);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 4; i++) {
                sb.append(readOutput());
            }
            String tokenOuput = sb.toString();
            String token = getAuthTokenFromOuput(tokenOuput);
            LOGGER.info("Parsed token is " + token);
            List<String> serverIps = getServerIps(token);
            LOGGER.info("Number of resolved active VMs Ips" + serverIps.size());
            if (serverIps.isEmpty()) {
                LOGGER.info("No ips, exiting...");
                return;
            }
            argument = argument / serverIps.size();
            if (argument > 95) {
                argument = 95;
            }
            LOGGER.info("Setting cpu load on each VM to " + argument);
            for (String s : serverIps) {
                LOGGER.info("Connecting to VM with ip" + s);
                SShVmAndSetCpuUsage.perform(s, rsaKeyFile, argument);
            }
            LOGGER.info("Module B is finishing work");
            channel.disconnect();
            session.disconnect();
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
        } finally {
            lockFile.delete();
            System.exit(0);
        }
    }

    private static void obtainLock() throws InterruptedException, IOException {
        int timeout = 0;
        while (lockFile.exists()) {
            LOGGER.info("LOCKED! Waiting!");
            Thread.sleep(60_000);
            timeout++;
            if (timeout > 10) {
                throw new IllegalStateException();
            }
        }
        LOGGER.info("Creating lock file.");
        lockFile.createNewFile();
    }

    private static List<String> getServerIps(String token) throws IOException, ParseException {
        LOGGER.info("Executing request for VMs IPs");
        String ending = "v2.0/floatingips";
        String beginning = PropertiesFromFile.getProperty(PropertiesFromFile.NETWORK_ENDPOINT);
        if (beginning.lastIndexOf('/') != beginning.length() - 1) {
            throw new IllegalArgumentException();
        }
        String url = beginning + ending;
        LOGGER.info("Server IPs GET URL" + url);
        HttpURLConnection con = (HttpURLConnection)
                new URL(url).openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("X-Auth-Token", token);
        int responseCode = con.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String out = response.toString();
        LOGGER.info(out);
        return getActiveFloatingIps(out);
    }

    private static List<String> getActiveFloatingIps(String out) throws ParseException {
        JSONParser parser = new JSONParser();
        List<String> result = new ArrayList<>();
        JSONObject obj = (JSONObject) parser.parse(out);
        JSONArray array = (JSONArray) obj.get("floatingips");
        for (Object floatingIp : array) {
            JSONObject obj2 = (JSONObject) floatingIp;
            String status = (String) obj2.get("status");
            String portId = (String) obj2.get("port_id");
            if (portId != null && "ACTIVE".equals(status)) {
                String ip = (String) obj2.get("floating_ip_address");
                result.add(ip);
                LOGGER.info("Resolved Active VM IP:" + ip);
            }
        }
        return result;
    }

    public static String readOutput() throws IOException {
        while (in.available() > 0) {
            int i = in.read(tmp, 0, 1024);
            if (i < 0) break;
            String output = new String(tmp, 0, i);
            return output;
        }
        return null;
    }

    private static void writeCommand(String command) throws IOException {
        System.out.println(command);
        out.write((command + "\n").getBytes());
        out.flush();
    }

    static String getAuthTokenFromOuput(String output) {//183 characters
        int tokenIdx = output.indexOf("| id         | ") + 15;
        return output.substring(tokenIdx, tokenIdx + 183);
    }
}
