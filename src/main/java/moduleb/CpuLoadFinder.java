package moduleb;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.log4j.Logger;

public class CpuLoadFinder {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    private static int ratio = 1000;

    public static int findCpuForDateTime(String dataFile, LocalDateTime targetDateTime) throws IOException {
        Logger.getLogger(CpuLoadFinder.class).info("Searching load for " + targetDateTime + " in file " + dataFile);
        File result = new File(dataFile);
        List<String> lines = Files.readAllLines(Paths.get(result.getAbsolutePath()), Charset.defaultCharset());
        for (String line : lines) {
            String[] values = line.split(",");
            int requests = Integer.parseInt(values[1]);
            LocalDateTime dateTime = LocalDateTime.parse(values[0], formatter);
            if (dateTime.isEqual(targetDateTime)) {
                Logger.getLogger(CpuLoadFinder.class).info("Found cpu load " + requests + " ratio is 1/" + ratio);
                return requests / ratio;
            }
        }
        String errMsg = "Not found load in file " + dataFile + " " + "for dateTime" + targetDateTime;
        Logger.getLogger(CpuLoadFinder.class).error(errMsg);
        throw new IllegalArgumentException(errMsg);
    }
}
