package moduleb;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public class SShVmAndSetCpuUsage {
    private static InputStream in;
    private static OutputStream out;
    private static Logger LOGGER = Logger.getLogger(SShVmAndSetCpuUsage.class);
    private static byte[] tmp = new byte[5000];

    public static synchronized void perform(String ip, File keyPair, int argument) {
        try {
            JSch jsch = new JSch();
            //jsch.setKnownHosts("/home/foo/.ssh/known_hosts");
            String host = ip;
            String user = "ubuntu";
            Session session = jsch.getSession(user, host, 22);
            jsch.addIdentity(keyPair.getAbsolutePath());
            UserInfo ui = new SshUserInfo();
            session.setUserInfo(ui);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(15_000);
            Channel channel = session.openChannel("shell");
            in = channel.getInputStream();
            out = channel.getOutputStream();
            channel.connect();
            Thread.sleep(3_000);
            readOutput();
            Thread.sleep(3_000);
            readOutput();
            LOGGER.info("Connected to " + ip + " successfully. Seaching for old stress-ng");
            writeCommand("ps -aux | grep stress-ng | grep -v \"grep\" | awk '{print $2}'");//
            Thread.sleep(4_000);
            String output = readOutput();
            output = output.replaceAll("[^0-9]+", " ");
            output = output.substring(output.indexOf(" 2 ")+3);
            String [] pids = output.trim().split(" ");
            LOGGER.info("Pids array: "+Arrays.toString(pids));
            if(pids.length ==1 && "".equals(pids[0])){
                LOGGER.info("Old stress-ng not found.");
            }else if (pids.length >0) {
                for (String pid : pids) {
                    if(pid.matches( "[0-9]+")){
                        LOGGER.warn("After killing first process the second one is expected to exit automatically.");
                        LOGGER.info("Killing old stress-ng. Pid is " + pid);
                        writeCommand("kill " + pid);
                    }
                }
            } else {
                LOGGER.info("Old stress-ng not found.");
            }
            Thread.sleep(3_000);
            readOutput();
            LOGGER.info("Executing new stress-ng process command... on ip" + ip);
            writeCommand("stress-ng -c 1 -l " + argument + " & ");
            //necessary ampersand
            //https://unix.stackexchange.com/questions/89483/keeping-a-process-running-after-putty-or-terminal-has-been-closed
            Thread.sleep(3_000);
            readOutput();
            LOGGER.info("Finishing job for IP" + ip);
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
        }
    }

    public static String readOutput() throws IOException {
        while (in.available() > 0) {
            int i = in.read(tmp, 0, 1024);
            if (i < 0) break;

            String output = new String(tmp, 0, i);
            LOGGER.info(output);
            return output;
        }
        return null;
    }

    private static void writeCommand(String command) throws IOException {
        LOGGER.info(command);
        out.write((command + "\n").getBytes());
        out.flush();
    }
}
