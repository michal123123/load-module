package moduleb;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class PropertiesFromFile {
    private File propFile;
    private static Map<String, String> propertiesMap;
    private static PropertiesFromFile singleton;
    private static Logger LOGGER = Logger.getLogger(PropertiesFromFile.class);

    public static String MODULE_B_DELAY =  "moduleBDelay";
    public static String LIBS_INSTALL_DELAY = "libsInstalationDelay";
    public static String NETWORK_ID = "networkId";
    public static String IMAGE_ID = "imageId";
    public static String KEYPAIR = "keyPair";
    public static String NETWORK_ENDPOINT = "networkEndpoint";
    public static String COMPUTE_ENDPOINT = "computeEndpoint";
    public static String USERNAME = "username";
    public static String PASSWD = "passwd";
    public static String HOST = "host";

    private PropertiesFromFile(File propFile) {
        LOGGER.info("Creating ");
        if (propFile == null || !propFile.exists()) {
            throw new IllegalArgumentException();
        }
        propertiesMap = new HashMap<>();
        this.propFile = propFile;
        readFile();
        if (propertiesMap.size() != 10) {
            throw new IllegalStateException("Not enough properties");
        }
    }

    public static synchronized PropertiesFromFile getInstance(File propFile) {
        if (singleton != null) {
            return singleton;
        }
        singleton = new PropertiesFromFile(propFile);
        return singleton;
    }

    public static String getProperty(String propName) {
        if (propertiesMap == null) {
            throw new IllegalStateException();
        }
        return propertiesMap.get(propName);
    }

    private void readFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(propFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] attributes = line.split(",");
                if (attributes.length != 2) {//single line shloud have 2 parts
                    throw new IllegalArgumentException("Incorrect propertiesMap file");
                }
                String name = attributes[0];
                String value = attributes[1];
                if (value != null) {
                    propertiesMap.put(name, value);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            throw new IllegalArgumentException();
        }
    }
}
